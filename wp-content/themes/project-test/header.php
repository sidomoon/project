<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<?php get_template_part( 'template-parts/head' ); ?>
</head>

<body <?php body_class("page-body"); ?>>

<div class="page-container">

	<header class="page-header">

		<div class="page-header__wrapper">

			<a class="page-header__logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">

				<?php $logo = CFS()->get( 'logo' ); ?>
				<?php if( ! empty($logo) ): ?>

					<img src="<?php echo CFS()->get( 'logo' ); ?>" width="50" height="50" alt="logo">

				<?php endif; ?>

			</a>

			<nav class="main-nav" role="navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_class' => 'main-nav__list', 'container' => false ) ); ?>
			</nav>

			<a class="page-header__phone" href="tel:1111111">
				Call us
			</a>

		</div>

	</header>
