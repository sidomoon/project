
## WP Theme Development
Cloning repository

Put [Wordpress](https://wordpress.org/download/) in the project folder 

Next, read the README.md in the theme folder

## Working with git
WP folders and files are in the .gitignore file.

For the ease of use, the installed plugins will be stored in the repository.

## Project Setup
development domain: `example.loc`

prefix for tables: `wp_`

